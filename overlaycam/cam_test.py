from time import sleep

import numpy as np
import requests
import cv2


# Get TensorFlow BodyMask from bodypix.js local webserver
def get_mask(frame, bodypix_url='http://localhost:9000'):
    _, data = cv2.imencode(".jpg", frame)
    r = requests.post(
        url=bodypix_url,
        data=data.tobytes(),
        headers={'Content-Type': 'application/octet-stream'})
    mask = np.frombuffer(r.content, dtype=np.uint8)
    mask = mask.reshape((frame.shape[0], frame.shape[1]))
    return mask

# setup access to the *real* webcam
cap = cv2.VideoCapture('/dev/video0')
height, width = 480, 640
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
cap.set(cv2.CAP_PROP_FPS, 60)

# setup the fake camera
# fake = pyfakewebcam.FakeWebcam('/dev/video20', width, height)

# load the virtual background
# background = cv2.imread("/data/background.jpg")
# background_scaled = cv2.resize(background, (width, height))

# frames forever
while True:
    success, frame = cap.read()
    # frame = get_frame(cap, background_scaled)
    # # fake webcam expects RGB
    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # fake.schedule_frame(frame)

    # read in a "virtual background" (should be in 16:9 ratio)
    replacement_bg_raw = cv2.imread("data/background.jpg")

    height, width = 480, 640
    # resize to match the frame (width & height from before)
    replacement_bg = cv2.resize(replacement_bg_raw, (width, height))

    mask = None
    while mask is None:
        try:
            mask = get_mask(frame)
        except requests.RequestException:
            print("mask request failed, retrying")
    # combine the background and foreground, using the mask and its inverse
    inv_mask = 1 - mask

    for c in range(frame.shape[2]):
        frame[:, :, c] = frame[:, :, c] * mask + replacement_bg[:, :, c] * inv_mask

    # Cam Test
    # sleep(5)
    cv2.imwrite("test.jpg", frame)