[https://elder.dev/posts/open-source-virtual-background/](https://elder.dev/posts/open-source-virtual-background/)





```
$ docker build -t fakecam ./fakecam


# create a network
docker network create --driver bridge fakecam

# start the bodypix app
docker run -d \
--name=bodypix \
--network=overlaycam \
-p 9000:9000 \
--shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 \
bodypix

# start the camera, note that we need to pass through video devices,
# and we want our user ID and group to have permission to them
# you may need to `sudo groupadd $USER video`

docker run -d \
--name=overlaycam \
--network=overlaycam \
-u "$(id -u):$(getent group video | cut -d: -f3)" \
$(find /dev -name 'video*' -printf "--device %p ") \
overlaycam



$ docker run -d \
--name=bodypix \
-p 9000:9000 \
--shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 \
bodypix
  

```


## Setup Fake Cam

```
$ sudo apt install v4l2loopback-dkms
$ sudo modprobe -r v4l2loopback
$ sudo modprobe v4l2loopback devices=1 video_nr=20 card_label="v4l2loopback" exclusive_caps=1


```

## Troubleshoot

### Resource busy after restart

```
OSError: [Errno 16] Device or resource busy: '/dev/video20'

```

If docker cannot connet to loopdevice, identify process that is locking the device:
```
$ lsof +D /dev

$ kill -9 <PID>

```


